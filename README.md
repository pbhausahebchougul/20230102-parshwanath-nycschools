# 20230102-Parshwanath-NYCSchools

### A app to display NYC Schools information

### Features

1. Display a list of NYC High Schools.
2. In the Details View Added These Details:
    - Schools SAT Scores
    - Schools Overview
    - Schools Contact Information
