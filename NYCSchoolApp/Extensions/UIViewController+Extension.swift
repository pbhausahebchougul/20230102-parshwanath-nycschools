//
//  UIViewController+Extension.swift
//  NYCSchoolApp
//
//  Created by Bhausaheb Chougule, Parshwanath on 01/06/23.
//

import UIKit

extension UIViewController {
    func showAlert( _ message: String ) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alert.addAction( UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
