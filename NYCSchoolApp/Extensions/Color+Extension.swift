//
//  Color+Extension.swift
//  NYC Schools App
//
//  Created by Bhausaheb Chougule, Parshwanath on 01/06/23.
//

import Foundation

import SwiftUI

public extension Color {

    /// Brand Colors
    static let nycAccentBlue = Color("nycAccentBlue")
    static let nycBlack = Color("nycBlack")
    static let nycDarkBronze = Color("nycDarkBronze")
    static let nycDarkGreen = Color("nycDarkGreen")
    static let nycDarkRed = Color("nycDarkRed")
    static let nycLightBlue = Color("nycLightBlue")
    static let nycLightBronze = Color("nycLightBronze")
    static let nycLightGreen = Color("nycLightGreen")
    static let nycNavBlue = Color("nycNavBlue")
    static let nycNavy = Color("nycNavy")
    static let nycNavyDark = Color("nycNavyDark")
    static let nycRed = Color("nycRed")
    static let nycAccentRed = Color("nycAccentRed")
    static let nycWhite = Color("nycWhite")
    static let nycGrayscaleLight = Color("nycGrayscaleLight")
    static let nycPrimaryRed = Color("nycPrimaryRed")

}

public extension UIColor {

    /// Brand Colors
    static let nycBlack = UIColor(.nycBlack)
    static let nycDarkBronze = UIColor(.nycDarkBronze)
    static let nycDarkGreen = UIColor(.nycDarkGreen)
    static let nycDarkRed = UIColor(.nycDarkRed)
    static let nycLightBlue = UIColor(.nycLightBlue)
    static let nycLightBronze = UIColor(.nycLightBronze)
    static let nycLightGreen = UIColor(.nycLightGreen)
    static let nycNavBlue = UIColor(.nycNavBlue)
    static let nycNavy = UIColor(.nycNavy)
    static let nycNavyDark = UIColor(.nycNavyDark)
    static let nycRed = UIColor(.nycRed)
    static let nycAccentRed = UIColor(.nycAccentRed)
    static let nycWhite = UIColor(.nycWhite)
    static let nycGrayscaleLight = UIColor(.nycGrayscaleLight)
    static let nycPrimaryRed = UIColor(.nycPrimaryRed)

}
