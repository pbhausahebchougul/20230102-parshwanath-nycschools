//
//  Font+Extension.swift
//  NYC Schools App
//
//  Created by Bhausaheb Chougule, Parshwanath on 31/05/23.
//

import UIKit

extension UIFont {

    static func large() -> UIFont {
        return aldaProBold(size: 35)
    }

    static func h1() -> UIFont {
        return aldaProBold(size: 30)
    }

    static func h2() -> UIFont {
        return aldaProRegular(size: 23)
    }

    static func h3() -> UIFont {
        return aldaProBold(size: 18)
    }

    static func h4() -> UIFont {
        return aldaProRegular(size: 18)
    }

    static func h6() -> UIFont {
        return aldaProRegular(size: 16)
    }

    static func h7() -> UIFont {
        return aldaProRegular(size: 13)
    }

    static func h8() -> UIFont {
        return aldaProBold(size: 15)
    }
    static func defaultFont() -> UIFont {
        return .systemFont(ofSize: 16)
    }
    private static func aldaProBold(size: CGFloat) -> UIFont {
        return UIFont(name: "AldaPro-Bold", size: size) ?? .defaultFont()
    }

    private static func aldaProBoldItalic(size: CGFloat) -> UIFont {
        return UIFont(name: "AldaPro-BoldItalic", size: size) ?? .defaultFont()
    }

    private static func aldaProRegular(size: CGFloat) -> UIFont {
        return UIFont(name: "AldaPro-Regular", size: size) ?? .defaultFont()
    }

    private  static func aldaProRegularItalic(size: CGFloat) -> UIFont {
        return UIFont(name: "AldaPro-RegularItalic", size: size) ?? .defaultFont()
    }
}
