//
//  Defaults.swift
//  NYCSchoolApp
//
//  Created by Bhausaheb Chougule, Parshwanath on 02/06/23.
//

import Foundation

class Defaults {
    struct UserDefaultsConstants {
        static let keyMockJSON = "mockJSON"
    }

    @UserDefaultsHandler(key: UserDefaultsConstants.keyMockJSON, defaultValue: false)
    var mockJSON: Bool

}
