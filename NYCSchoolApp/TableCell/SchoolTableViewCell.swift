//
//  SchoolTableViewCell.swift
//  NYC Schools App
//
//  Created by Bhausaheb Chougule, Parshwanath on 19/05/23.
//

import UIKit

class SchoolTableViewCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var campusNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var webSiteLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var studentCountLabel: UILabel!
    @IBOutlet weak var borderView: ShadowView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setUpDesign()
        setFonts()
        setColors()

    }

    /// Populate school value on cell UI
    /// - Parameter school: SchoolListCellViewModel
    func updateCell(with school: SchoolListCellViewModel) {
        schoolNameLabel.text = school.name
        campusNameLabel.text = school.campusName
        emailLabel.text = school.email
        addressLabel.text = school.address
        webSiteLabel.text = school.webSite
        timeLabel.text = school.schoolTime
        studentCountLabel.text = school.studentCount
    }

    /// Set font to label
    private func setFonts() {
        schoolNameLabel.font = .h3()
        campusNameLabel.font = .h6()
        emailLabel.font = .h7()
        addressLabel.font = .h7()
        webSiteLabel.font = .h7()
        timeLabel.font = .h7()
        studentCountLabel.font = .h7()

    }

    /// Set colors to label
    private func setColors() {
        schoolNameLabel.textColor = .nycBlack
        campusNameLabel.textColor = .nycDarkBronze
        emailLabel.textColor = .nycNavy
        addressLabel.textColor = .nycBlack
        webSiteLabel.textColor = .nycNavy
        timeLabel.textColor = .nycBlack
        studentCountLabel.textColor = .nycBlack

    }

    /// Set cell design
    private func setUpDesign() {
        borderView.layer.borderColor = UIColor.nycGrayscaleLight.cgColor
        borderView.layer.cornerRadius = 8.0
        borderView.setShadow(shadowRadiusOverride: 0.5, shadowOpacityOverride: 0.2)
    }
}
