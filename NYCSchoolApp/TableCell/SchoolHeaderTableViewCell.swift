//
//  SchoolHeaderTableCell.swift
//  NYC Schools App
//
//  Created by Bhausaheb Chougule, Parshwanath on 31/05/23.
//

import UIKit

class SchoolHeaderTableViewCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var campusNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setFonts()
    }
    /// Populate school header and description values to cell UI
    /// - Parameter schoolDetails: SchoolHeaderDetailsViewModel
    func displayHeaderDetails(schoolDetails: SchoolDetailCellViewModel) {
        guard let schoolDetails = schoolDetails as? SchoolHeaderDetailsViewModel else { return }
        schoolNameLabel.text = Constant.description
        descriptionLabel.text = schoolDetails.overview
        campusNameLabel.text = schoolDetails.campusName
    }
    /// Set font to label
    private func setFonts() {
        schoolNameLabel.font = .h3()
        descriptionLabel.font = .h6()
        campusNameLabel.font = .h4()
    }
}
