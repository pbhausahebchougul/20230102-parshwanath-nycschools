//
//  SchoolSATTableCell.swift
//  NYC Schools App
//
//  Created by Bhausaheb Chougule, Parshwanath on 31/05/23.
//

import UIKit

// common table view cell
class SchoolSATTableViewCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var criticalReadingAvgScoreLabel: UILabel!
    @IBOutlet weak var mathsAvgScoreLabel: UILabel!
    @IBOutlet weak var writingAVGScoreLabel: UILabel!

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var noOfTestTakersLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setFonts()
        setSATColor()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func displaySatDetails(viewModel: SchoolDetailCellViewModel) {
            guard let viewModel = viewModel as? SatDetailsViewModel else { return }
        titleLabel.text = Constant.satDetailTitle
        noOfTestTakersLabel.text = viewModel.noOfTestTakers
        criticalReadingAvgScoreLabel.text = viewModel.criticalReadingAvgScore
        mathsAvgScoreLabel.text = viewModel.mathsAvgScore
        writingAVGScoreLabel.text = viewModel.writingAVGScore
    }

    /// Set font to label
    private func setFonts() {
        titleLabel.font = .h3()
        noOfTestTakersLabel.font = .h7()
        criticalReadingAvgScoreLabel.font = .h7()
        mathsAvgScoreLabel.font = .h7()
        writingAVGScoreLabel.font = .h7()
    }

    /// Set colors to label
    private func setSATColor() {
        titleLabel.textColor = .nycNavyDark
        noOfTestTakersLabel.textColor = .nycNavy
        criticalReadingAvgScoreLabel.textColor = .nycNavy
        mathsAvgScoreLabel.textColor = .nycNavy
        writingAVGScoreLabel.textColor = .nycNavy
    }

}
