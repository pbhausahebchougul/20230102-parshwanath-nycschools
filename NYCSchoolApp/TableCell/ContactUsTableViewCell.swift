//
//  ContactUsTableViewCell.swift
//  NYCSchoolApp
//
//  Created by Bhausaheb Chougule, Parshwanath on 02/06/23.
//

import UIKit

class ContactUsTableViewCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet weak var addressButton: UIButton!
    @IBOutlet weak var emailButton: UIButton!
    @IBOutlet weak var phoneNumberButton: UIButton!
    @IBOutlet weak var webSiteButton: UIButton!

    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var webSiteLabel: UILabel!

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setFonts()
        self.setContactUsColor()
    }

    /// Populate address values on cell UI
    /// - Parameter viewModel: SchoolContactViewModel
    func displayAddress(viewModel: SchoolDetailCellViewModel) {
        guard let viewModel = viewModel as? SchoolContactViewModel else { return }
        titleLabel.text = Constant.contactTitle
        addressLabel.text = viewModel.address
        emailLabel.text = viewModel.email
        phoneNumberLabel.text = viewModel.phoneNumber
        webSiteLabel.text = viewModel.website
    }

    /// Set font to label
    private func setFonts() {
        titleLabel.font = .h3()
        addressLabel.font = .h7()
        emailLabel.font = .h7()
        phoneNumberLabel.font = .h7()
        webSiteLabel.font = .h7()
    }

    /// Set colors to label
    private func setContactUsColor() {
        titleLabel.textColor = .nycNavyDark
        addressLabel.textColor = .nycNavy
        emailLabel.textColor = .nycNavy
        phoneNumberLabel.textColor = .nycNavBlue
        webSiteLabel.textColor = .nycNavBlue
    }
}
