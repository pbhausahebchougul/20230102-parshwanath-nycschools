//
//  SATDetails.swift
//  NYC Schools App
//
//  Created by Bhausaheb Chougule, Parshwanath on 31/05/23.
//

import Foundation

/// School SAT Details data model holds sat details values
struct SchoolSATDetails: Decodable {
    var dbn: String?
    var schoolName: String?
    var noOfTestTakers: String?
    var criticalReadingAvgScore: String?
    var mathsAvgScore: String?
    var writingAVGScore: String?

    enum CodingKeys: String, CodingKey {
        case dbn = "dbn"
        case schoolName = "school_name"
        case noOfTestTakers = "num_of_sat_test_takers"
        case criticalReadingAvgScore = "sat_critical_reading_avg_score"
        case mathsAvgScore = "sat_math_avg_score"
        case writingAVGScore = "sat_writing_avg_score"

    }
}
