//
//  School.swift
//  NYC Schools App
//
//  Created by Bhausaheb Chougule, Parshwanath on 19/05/23.
//

import Foundation

/// School data model to hold school object values
struct School: Decodable {
    var dbn: String?
    var schoolName: String?
    var overview: String?
    var startTime: String?
    var endTime: String?
    var totalStudent: String?
    var email: String?
    var phoneNumber: String?
    var webSite: String?
    var campusName: String?
    var primaryAddressLine1: String?
    var city: String?
    var stateCode: String?
    var zip: String?
    var totalStudens: String?

    enum CodingKeys: String, CodingKey {
        case dbn = "dbn"
        case schoolName = "school_name"
        case overview = "overview_paragraph"
        case startTime = "start_time"
        case endTime = "end_time"
        case email = "school_email"
        case phoneNumber = "phone_number"
        case webSite = "website"
        case campusName = "campus_name"
        case primaryAddressLine1 = "primary_address_line_1"
        case city = "city"
        case stateCode = "state_code"
        case zip
        case totalStudens = "total_students"
    }
}

extension School {

    /// Create full address of school
    /// - Returns: String school full address string
    func fullAddress() -> String {
        var address = ""
        if let primaryAddress = self.primaryAddressLine1 {
            address.append("\(primaryAddress)")
        }
        if let city = self.city {
            address.append(", \(city)")
        }
        if let state = self.stateCode {
            address.append(", \(state)")
        }
        if let zip = self.zip {
            address.append(", \(zip)")
        }
        return address
    }

    func schoolTiming() -> String {
        var timing = "Timing: "
        if let startTime = self.startTime {
            timing.append("\(startTime)")
        } else {
            timing.append("NA")
        }

        if let endTime = self.endTime {
            timing.append(" to \(endTime)")
        }

        return timing
    }
}
