//
//  SchoolViewModel.swift
//  NYCSchoolApp
//
//  Created by Bhausaheb Chougule, Parshwanath on 01/06/23.
//

import Foundation

/// SchoolListCellViewModel type hold values to display on UI
struct SchoolListCellViewModel {

    // MARK: - Class Property
    let dbn: String
    let name: String
    var campusName: String
    var address: String
    let email: String
    var schoolTime: String
    var studentCount: String
    let webSite: String
}

/// SchoolViewModel class
class SchoolViewModel {

    // MARK: - Class Property
    var schools: [School] = [School]()
    private var fetchSession: URLSessionDataTask?
    var reloadTableView: (() -> Void)?
    var showError: (() -> Void)?
    var showLoading: (() -> Void)?
    var hideLoading: (() -> Void)?
    private var serviceRequest: SchoolServiceRequestType

    /// cellViewModel computed property hold SchoolListCellViewModel collection
    private var cellViewModels: [SchoolListCellViewModel] = [SchoolListCellViewModel]() {
        didSet {
            self.reloadTableView?()
        }
    }

    init(serviceRequest: SchoolServiceRequestType) {
        self.serviceRequest = serviceRequest
    }

    /// Fetch school list
    func fetchSchools() {
        if !(fetchSession?.progress.isFinished ?? true) {
            return
        }
        showLoading?()
        fetchSession = serviceRequest.fetchSchools(localeFileName: nil) { [weak self] apiResult in
            self?.fetchSession = nil
            guard let self = self else { return }
            self.hideLoading?()
            switch apiResult {
            case .success(let schoolList):
                self.createCell(schools: schoolList)
            case .failure(let error):
                Logger.log(message: error.localizedDescription)
                self.showError?()
                break
            }
        }
    }

    /// Hold number of cells count
    var numberOfCells: Int {
        return cellViewModels.count
    }

    /// Returns selected index cell view model
    /// - Parameter indexPath: IndexPath selected index path
    /// - Returns: SchoolListCellViewModel
    func getCellViewModel( at indexPath: IndexPath ) -> SchoolListCellViewModel? {
        return cellViewModels.count > 0 ? cellViewModels[indexPath.row] : nil
    }

    /// Create SchoolListCellViewModel collection
    /// - Parameter schools: [School] school collection
    func createCell(schools: [School]) {
        self.schools = schools
        let schoolViewModel: [SchoolListCellViewModel] = schools.map({SchoolListCellViewModel(dbn: $0.dbn ??  Constant.defaultEmptyString, name: $0.schoolName ?? Constant.defaultEmptyString,
                                                                                              campusName: $0.campusName ?? Constant.defaultEmptyString,
                                                                                              address: $0.fullAddress(),
                                                                                              email: $0.email ?? Constant.noValue,
                                                                                              schoolTime: $0.schoolTiming(),
                                                                                              studentCount: "\(Constant.studentCountTitle) \($0.totalStudens ?? Constant.noValue)",
                                                                                              webSite: $0.webSite ?? Constant.noValue)})

        cellViewModels = schoolViewModel
    }

    /// search selected school
    /// - Parameter dbn: String dbn
    /// - Returns: School school object
    func searchSelectedSchool(dbn: String) -> School {
        return schools.first(where: { $0.dbn?.caseInsensitiveCompare(dbn) == .orderedSame }) ?? School()
    }
}
