//
//  SchoolListViewController.swift
//  NYC Schools App
//
//  Created by Bhausaheb Chougule, Parshwanath on 19/05/23.
//  Copyright (c) 2023 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

/// SchoolListViewController type to display school list
class SchoolListViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    // MARK: - Class Property
    let schoolViewModel: SchoolViewModel = SchoolViewModel(serviceRequest: SchoolServiceRequests())

    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpViewModel()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = Constant.NYCSchoolList
    }

    // MARK: - Intialize and request data from SchoolListViewModel

    /// Set up view model
    func setUpViewModel() {
        schoolViewModel.reloadTableView = {
            DispatchQueue.main.async { self.tableView.reloadData() }
        }
        schoolViewModel.showError = {
            DispatchQueue.main.async { self.showAlert("Ups, something went wrong.") }
        }
        schoolViewModel.showLoading = {
            DispatchQueue.main.async { self.activityIndicator.startAnimating() }
        }
        schoolViewModel.hideLoading = {
            DispatchQueue.main.async { self.activityIndicator.stopAnimating() }
        }
        schoolViewModel.fetchSchools()
    }

    /// Route to school detail view
    /// - Parameter selectedIndex: IndexPath selected school index path
    func routeToSchollDetails(selectedIndex: IndexPath) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let destinationVC = storyboard.instantiateViewController(withIdentifier: "SchoolDetailsViewController") as? SchoolDetailsViewController else {
            return
        }
        guard let dbn = schoolViewModel.getCellViewModel(at: selectedIndex)?.dbn else { return }
        destinationVC.schoolDetailViewModel.setSchoolDetails(school: schoolViewModel.searchSelectedSchool(dbn: dbn))
            navigationController?.pushViewController(destinationVC, animated: true)
    }

}

extension SchoolListViewController: UITableViewDataSource {
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return schoolViewModel.numberOfCells
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           guard let schoolCell = tableView.dequeueReusableCell(withIdentifier: "SchoolTableViewCell", for: indexPath) as? SchoolTableViewCell
                else {
                    return UITableViewCell(frame: CGRect.zero)
                }
            if let cellModel = schoolViewModel.getCellViewModel(at: indexPath) {
                schoolCell.updateCell(with: cellModel)
            }
            return schoolCell
        }
}

extension SchoolListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        routeToSchollDetails(selectedIndex: indexPath)
    }
}
