//
//  SchoolDetailViewModel.swift
//  NYCSchoolApp
//
//  Created by Bhausaheb Chougule, Parshwanath on 01/06/23.
//

import Foundation

/// SchoolDetailsCellType enum type
enum SchoolDetailsCellType {
    case header
    case satDetails
    case address
}

/// Protocol SchoolDetailCellViewModel type hold SchoolDetailsCellType
protocol SchoolDetailCellViewModel {
    var type: SchoolDetailsCellType { get set}
}

/// SatDetailsViewModel type hold sat details to be displayed
struct SatDetailsViewModel: SchoolDetailCellViewModel {
    var type: SchoolDetailsCellType
    var noOfTestTakers: String
    var criticalReadingAvgScore: String
    var mathsAvgScore: String
    var writingAVGScore: String
}

/// SchoolHeaderDetailsViewModel type hold header values to be displayed
struct SchoolHeaderDetailsViewModel: SchoolDetailCellViewModel {
    var type: SchoolDetailsCellType
    var name: String
    var overview: String
    var campusName: String
    var descriptionTile: String
}

/// SchoolContactViewModel type hold school contact values to be displayed
struct SchoolContactViewModel: SchoolDetailCellViewModel {
    var type: SchoolDetailsCellType
    var email: String
    var phoneNumber: String
    var website: String
    var address: String
}

/// SchoolDetailViewModel data view model
class SchoolDetailViewModel {
    private var school: School = School()
    private var satDetails: SchoolSATDetails = SchoolSATDetails()
    private var fetchSession: URLSessionDataTask?
    private var serviceRequest: SchoolServiceRequestType
    var reloadTableView: (() -> Void)?
    init(serviceRequest: SchoolServiceRequestType) {
        self.serviceRequest = serviceRequest
    }
    private var cellViewModels: [SchoolDetailCellViewModel] = [SchoolDetailCellViewModel]() {
        didSet {
            self.reloadTableView?()
        }
    }
    /// Set school details
    ///  /// - Parameter school: School object type
    func setSchoolDetails(school: School) {
        self.school = school
    }
    /// Return school name
    /// - Returns: String school name
    func getSchoolName() -> String {
        self.school.schoolName ?? Constant.defaultEmptyString
    }
    /// Display school details
    func displaySchoolDetails() {
        self.createCell(satDetails: [])
        if let reloadTable = self.reloadTableView {
            reloadTable()
        }
    }
    /// fetch SAT details
    func fetchSATDetails() {
        if !(fetchSession?.progress.isFinished ?? true) {
            return
        }
        guard let dbn: String = school.dbn else { return }
        fetchSession = serviceRequest.fetchSchoolSATDetails(dbn: dbn, localeFileName: nil) { [weak self] apiResult in
            self?.fetchSession = nil
            guard let self = self else { return }
            switch apiResult {
            case .success(let satDetails):
                self.createCell(satDetails: satDetails)
            case .failure(let error):
                Logger.log(message: error.localizedDescription)
            }
        }
    }
    /// number of cells
    var numberOfCells: Int {
        return cellViewModels.count
    }
    /// Retuens selectred index cell view model
    /// - Parameter indexPath: IndexPath selected index path
    /// - Returns: SchoolDetailCellViewModel school details cell view model object
    func getCellViewModel( at indexPath: IndexPath ) -> SchoolDetailCellViewModel {
        return cellViewModels[indexPath.row]
    }

    /// Crete cell view model collection
    /// - Parameter satDetails: [SchoolSATDetails] sat details collection object
    func createCell(satDetails: [SchoolSATDetails]) {
        var schoolDetailsViewModel = [SchoolDetailCellViewModel]()
        
        let schoolHeader: SchoolDetailCellViewModel = createHeaderViewModel()
        let schoolContacts: SchoolDetailCellViewModel = createContactViewModel()
        
        schoolDetailsViewModel.append(schoolHeader)
        schoolDetailsViewModel.append(schoolContacts)
        
        if let satDetails = satDetails.first {
            let satDetails: SchoolDetailCellViewModel = createSATViewModel(satDetails: satDetails)
            schoolDetailsViewModel.insert(satDetails, at: schoolDetailsViewModel.count > 0 ? 1 : 0)
        }
        cellViewModels = schoolDetailsViewModel
    }
    
    
    /// Create and return SchoolContactViewModel
    /// - Returns: SchoolContactViewModel
    private func createHeaderViewModel() -> SchoolHeaderDetailsViewModel {
        return SchoolHeaderDetailsViewModel(type: .header, name: self.school.schoolName ?? Constant.defaultEmptyString,
                                            overview: self.school.overview ?? Constant.defaultEmptyString,
                                            campusName: self.school.campusName ?? Constant.defaultEmptyString,
                                            descriptionTile: Constant.descriptionTitle)
    }
    
    /// Create and return SchoolContactViewModel
    /// - Returns: SchoolContactViewModel
    private func createContactViewModel() -> SchoolContactViewModel {
        return SchoolContactViewModel(type: .address, email: self.school.email ?? Constant.defaultEmptyString,
                                                                             phoneNumber: self.school.phoneNumber ?? Constant.defaultEmptyString,
                                                                             website: self.school.webSite ?? Constant.defaultEmptyString,
                                                                             address: self.school.fullAddress())
    }
    
    /// Create and return SatDetailsViewModel
    /// - Parameter satDetails: SchoolSATDetails data model
    /// - Returns: SatDetailsViewModel
    private func createSATViewModel(satDetails: SchoolSATDetails) -> SatDetailsViewModel {
        let noOfTestTakers = "\(Constant.numberOfTestTakers)\(satDetails.noOfTestTakers ?? Constant.defaultEmptyString)"
        let criticalReadingAvgScore = "\(Constant.criticalReadingAvgScore)\(satDetails.criticalReadingAvgScore ?? Constant.defaultEmptyString)"
        let mathsAvgScore = "\(Constant.mathsAvgScore)\(satDetails.mathsAvgScore ?? Constant.defaultEmptyString)"
        let writingAVGScore = "\(Constant.writingAvgScore)\(satDetails.writingAVGScore ?? Constant.defaultEmptyString)"
        return SatDetailsViewModel(type: .satDetails, noOfTestTakers: noOfTestTakers, criticalReadingAvgScore: criticalReadingAvgScore, mathsAvgScore: mathsAvgScore, writingAVGScore: writingAVGScore)
    }
}
