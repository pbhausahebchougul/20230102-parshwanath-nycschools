//
//  SchoolDetailsViewController.swift
//  NYC Schools App
//
//  Created by Bhausaheb Chougule, Parshwanath on 30/05/23.
//  Copyright (c) 2023 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

/// School Details View Controller type display school details on UI
class SchoolDetailsViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!

    // MARK: - Class Property
    let schoolDetailViewModel: SchoolDetailViewModel = SchoolDetailViewModel(serviceRequest: SchoolServiceRequests())

    // MARK: - View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpViewModel()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = schoolDetailViewModel.getSchoolName()
    }
    // MARK: - request data from SchoolDetailsViewModel

    /// Set the view model
    func setUpViewModel() {
        schoolDetailViewModel.reloadTableView = {
            DispatchQueue.main.async { self.tableView.reloadData() }
        }

        schoolDetailViewModel.displaySchoolDetails()
        schoolDetailViewModel.fetchSATDetails()
    }

}

extension SchoolDetailsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolDetailViewModel.numberOfCells
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let schoolDetails = schoolDetailViewModel.getCellViewModel(at: indexPath)
        switch schoolDetails.type {
        case .header:
            guard let schoolHeaderCell = tableView.dequeueReusableCell(withIdentifier: "SchoolHeaderTableViewCell", for: indexPath) as? SchoolHeaderTableViewCell else {
                return UITableViewCell(frame: CGRect.zero)
            }
                schoolHeaderCell.displayHeaderDetails(schoolDetails: schoolDetails)
            return schoolHeaderCell
        case .satDetails:
            guard let satDetailsCell = tableView.dequeueReusableCell(withIdentifier: "SchoolSATTableViewCell", for: indexPath) as? SchoolSATTableViewCell else {
                return UITableViewCell(frame: CGRect.zero)
            }
                satDetailsCell.displaySatDetails(viewModel: schoolDetails)
            return satDetailsCell
        case .address:
            guard let addressDetailsCell = tableView.dequeueReusableCell(withIdentifier: "ContactUsTableViewCell", for: indexPath) as? ContactUsTableViewCell else {
                return UITableViewCell(frame: CGRect.zero)
            }
                addressDetailsCell.displayAddress(viewModel: schoolDetails)
            return addressDetailsCell
        }
    }
}
