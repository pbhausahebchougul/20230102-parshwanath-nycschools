//
//  Constant.swift
//  NYC Schools App
//
//  Created by Bhausaheb Chougule, Parshwanath on 01/06/23.
//

import Foundation

struct Constant {
    static let NYCSchoolList = "NYC Schools"
    static let description = "Description"
    static let defaultEmptyString = ""
    static let noValue = "NA"
    static let campusNameTtile = "Campus Name - "
    static let studentCountTitle = "Student Count - "
    static let numberOfTestTakers = "Number of test takers - "
    static let criticalReadingAvgScore = "Critical reading avg score - "
    static let mathsAvgScore = "Maths avg score - "
    static let writingAvgScore = "Writing avg score - "
    static let descriptionTitle = "Description"
    static let contactTitle = "Contact Us: "
    static let satDetailTitle = "SAT Details : "
}
