//
//  ShadowView.swift
//  NYC Schools App
//
//  Created by Bhausaheb Chougule, Parshwanath on 01/06/23.
//

import Foundation
import UIKit

final class ShadowView: UIView, Shadow {

    public var shadowRadius: CGFloat?
    public var shadowOpacity: Float?

    override func layoutSubviews() {
        super.layoutSubviews()

        setShadow(shadowRadiusOverride: shadowRadius, shadowOpacityOverride: shadowOpacity)
    }
}

protocol Shadow: UIView {
    func setShadow(shadowRadiusOverride: CGFloat?, shadowOpacityOverride: Float?)
}

extension Shadow {
    func setShadow(shadowRadiusOverride: CGFloat?, shadowOpacityOverride: Float?) {
        var cornerRadius = layer.cornerRadius

        if cornerRadius < 1 {
            cornerRadius = 10.0
        }

        // The shadow is different depending on how rounded the view is
        // If the view is completely round we use
        var shadowRadius: CGFloat = 5.0
        var shadowOpacity: Float = 0.3

        shadowRadius = shadowRadiusOverride ?? shadowRadius
        shadowOpacity = shadowOpacityOverride ?? shadowOpacity

        let shadowPath = UIBezierPath(roundedRect: bounds,
                                      cornerRadius: cornerRadius)
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 6.5)
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
        layer.shadowPath = shadowPath.cgPath

    }
}
