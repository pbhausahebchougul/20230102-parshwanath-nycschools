//
//  Logger.swift
//  NYC Schools App
//
//  Created by Bhausaheb Chougule, Parshwanath on 01/06/23.
//

import Foundation

public class Logger {
    /// print message only in debug mode
    /// - Parameter message: text that we want to print
    public static func log(message: String) {
        #if DEBUG
        // we only print logs if we are in debug mode
        print(message)
        #endif
    }

    /// debugPrint adds additional information that is useful for debugging like type information OR
    ///  If you make network call and do a debugPrint(response) instead of print(response), you will get a lot more valuable information
    /// - Parameter text: Any type of data we can pass here to print
    public static func logDetails(text: Any) {
        #if DEBUG
        // we only print logs if we are in debug mode
        debugPrint(text)
        #endif
    }
}
