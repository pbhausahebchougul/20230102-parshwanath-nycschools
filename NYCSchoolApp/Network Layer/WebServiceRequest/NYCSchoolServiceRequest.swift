//
//  NYCSchoolServiceRequest.swift
//  NYC Schools App
//
//  Created by Bhausaheb Chougule, Parshwanath on 19/05/23.
//

import Foundation

typealias SchoolsListAPIResponse = (Result<[School], Error>) -> Void
typealias SchoolSATAPIResponse = (Result<[SchoolSATDetails], Error>) -> Void

/// Protocol of School Service Request Type
protocol SchoolServiceRequestType {

    /// Fetch school api service request to get school list
    /// - Parameter completion: @escaping SchoolsResponse  escaping coloure block
    /// - Returns: URLSessionDataTask? return url session object of type optional
    @discardableResult func fetchSchools(localeFileName: String?, completion: @escaping SchoolsListAPIResponse) -> URLSessionDataTask?

    /// Fetch school SAT api service request to get school sat details
    /// - Parameters:
    ///   - dbn: String dbn
    ///   - completion: @escaping SchoolSATResponse  escaping coloure block
    /// - Returns: URLSessionDataTask? return url session object of type optional
    @discardableResult func fetchSchoolSATDetails(dbn: String, localeFileName: String?, completion: @escaping SchoolSATAPIResponse) -> URLSessionDataTask?
}

/// School Service Requests type to create api request call
struct SchoolServiceRequests: SchoolServiceRequestType {

    @discardableResult func fetchSchools(localeFileName: String?, completion: @escaping SchoolsListAPIResponse) -> URLSessionDataTask? {
        let contactRequestModel = APIRequestModel(api: SchoolAPI.getSchoolList)
        return WebserviceHelper.requestAPI(apiModel: contactRequestModel, localeFileName: localeFileName) { response in
            switch response {
            case .success(let serverData):
                JSONResponseDecoder.decodeFrom(serverData, returningModelType: [School].self, completion: { (allRestaurantResponse, error) in
                    if let parserError = error {
                        completion(.failure(parserError))
                        return
                    }

                    if let restaurantResponse = allRestaurantResponse {
                        completion(.success(restaurantResponse ))
                        return
                    }
                })
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    @discardableResult func fetchSchoolSATDetails(dbn: String, localeFileName: String?, completion: @escaping SchoolSATAPIResponse) -> URLSessionDataTask? {
        let contactRequestModel = APIRequestModel(api: SchoolAPI.getSATDetails(dbn: dbn))
        return WebserviceHelper.requestAPI(apiModel: contactRequestModel, localeFileName: localeFileName) { response in
            switch response {
            case .success(let serverData):
                JSONResponseDecoder.decodeFrom(serverData, returningModelType: [SchoolSATDetails].self, completion: { (satDetailsResponse, error) in
                    if let parserError = error {
                        completion(.failure(parserError))
                        return
                    }

                    if let satDetails = satDetailsResponse {
                        completion(.success(satDetails))
                        return
                    }
                })
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
