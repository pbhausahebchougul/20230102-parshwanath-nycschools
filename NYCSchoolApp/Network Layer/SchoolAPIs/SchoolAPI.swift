//
//  SchoolAPI.swift
//  NYC Schools App
//
//  Created by Bhausaheb Chougule, Parshwanath on 19/05/23.
//

import Foundation

/// School API enum type
enum SchoolAPI {
    case getSchoolList
    case getSATDetails(dbn: String)
}

extension SchoolAPI: APIProtocol {

    /// Returns api service request type on the basis of SchoolAPI type
    /// - Returns: HTTPMethodType
    func httpMthodType() -> HTTPMethodType {
        var methodType = HTTPMethodType.get
        switch self {
        case .getSchoolList, .getSATDetails:
            methodType = .get
        }
        return methodType
    }

    /// Create api endpath on the basis of SchoolAPI type
    /// - Returns: String return api end path string
    func apiEndPath() -> String {
        var apiEndPath = ""
        switch self {
        case .getSchoolList:
            apiEndPath += WebserviceConstants.DOEHighSchool
            break
        case .getSATDetails(let dbn):
            apiEndPath += String(format: WebserviceConstants.SAT, dbn)
        }
        return apiEndPath
    }

    /// Returns api service basepath  on the basis of SchoolAPI type
    /// - Returns: String api base path
    func apiBasePath() -> String {
        switch self {
        case .getSchoolList, .getSATDetails:
            return WebserviceConstants.baseURL
        }
    }

}
