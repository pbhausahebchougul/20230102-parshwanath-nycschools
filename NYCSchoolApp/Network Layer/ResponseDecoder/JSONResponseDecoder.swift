//
//  JSONResponseDecoder.swift
//  NYC Schools App
//
//  Created by Bhausaheb Chougule, Parshwanath on 19/05/23.
//

import Foundation

/// JSON Response Decoder type to pase the json object to model type
class JSONResponseDecoder {

    typealias JSONDecodeCompletion<T> = (T?, Error?) -> Void

    /// Decode json object to model type
    /// - Parameters:
    ///   - responseData: Data response data
    ///   - returningModelType: T.Type model type
    ///   - completion: JSONDecodeCompletion<T>  hold completion block of decoded json object model and error
    static func decodeFrom<T: Decodable>(_ responseData: Data, returningModelType: T.Type, completion: JSONDecodeCompletion<T>) {
        do {
            let model = try JSONDecoder().decode(returningModelType, from: responseData)
            completion(model, nil)
        } catch let DecodingError.dataCorrupted(context) {
            Logger.log(message: "Data corrupted: \(context)")
            completion(nil, DecodingError.dataCorrupted(context))
        } catch let DecodingError.keyNotFound(key, context) {
            Logger.log(message: "Key '\(key)' not found: \(context.debugDescription) \n codingPath:\(context.codingPath)")
            completion(nil, DecodingError.keyNotFound(key, context))
        } catch let DecodingError.valueNotFound(value, context) {
            Logger.log(message: "Value '\(value)' not found: \(context.debugDescription) \n codingPath:\(context.codingPath)")
            completion(nil, DecodingError.valueNotFound(value, context))
        } catch let DecodingError.typeMismatch(type, context) {
            Logger.log(message: "Type '\(type)' not found: \(context.debugDescription) \n codingPath:\(context.codingPath)")
            completion(nil, DecodingError.typeMismatch(type, context))
        } catch {
            Logger.log(message: error.localizedDescription)
            completion(nil, error)
        }
    }

    /*func loadJson(filename fileName: String) -> [Person]? {
        if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode(ResponseData.self, from: data)
                return jsonData.person
            } catch {
                print("error:\(error)")
            }
        }
        return nil
    }*/

}
