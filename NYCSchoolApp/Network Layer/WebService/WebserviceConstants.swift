//
//  WebserviceConstants.swift
//  NYC Schools App
//
//  Created by Bhausaheb Chougule, Parshwanath on 19/05/23.
//

import Foundation

/// Webservice Constants type hold api serice url's
enum WebserviceConstants {
    static let baseURL = "https://data.cityofnewyork.us/resource"
    static let DOEHighSchool = "/s3k6-pzi2.json"
    static let SAT = "/f9bf-2cp4.json?dbn=%@"
}
