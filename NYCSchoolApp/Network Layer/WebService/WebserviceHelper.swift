//
//  WebserviceHelper.swift
//  NYC Schools App
//
//  Created by Bhausaheb Chougule, Parshwanath on 19/05/23.
//

import Foundation

enum NetworkError: Error {
    case incorrectData(Data)
    case incorrectURL
    case unknown
}

typealias WebServiceCompletionBlock = (Result<Data, Error>) -> Void

/// Helper class to prepare request(adding headers & clubbing base URL) & perform API request.
struct WebserviceHelper {

    /// Performs a API request which is called by any service request class.
    /// It also performs an additional task of validating the auth token and refreshing if necessary
    ///
    /// - Parameters:
    ///   - apiModel: APIModelType which contains the info about api endpath, header & http method type.
    ///   - completion: Request completion handler.
    /// - Returns: Returns a URLSessionDataTask instance.
    @discardableResult public static func requestAPI(apiModel: APIModelType, localeFileName: String? = nil, completion: @escaping WebServiceCompletionBlock) -> URLSessionDataTask? {

        let request = WebserviceConfig().getRequest(apiModel: apiModel)
#if DEBUG
        if Defaults().mockJSON {
            if let fileName = localeFileName, !fileName.isEmpty {
                loadLocal(assetName: fileName, completion: completion)
            }
            return nil
        }
#endif

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                Logger.logDetails(text: error ?? NetworkError.unknown)
                completion(.failure(error ?? NetworkError.unknown))
                return
            }

            if let httpStatus = response as? HTTPURLResponse, ![200, 201].contains(httpStatus.statusCode) {
                completion(.failure(NetworkError.incorrectData(data)))
            }
            Logger.log(message: String(decoding: data, as: UTF8.self))
            completion(.success(data))

        }

        task.resume()
        return task
    }

}

extension WebserviceHelper {

    static func loadLocal(assetName: String, completion: @escaping WebServiceCompletionBlock) {
        DispatchQueue.global(qos: .background).async {
            if let url = Bundle.main.url(forResource: assetName, withExtension: "json") {
                Logger.log(message: "Loading saved response for: \(assetName)")
                do {
                    let data = try Data(contentsOf: url)
                    sleep(1)
                    completion(.success(data))
                } catch {
                    completion(.failure(NetworkError.unknown))
                }

            } else {
                completion(.failure(NetworkError.unknown))
            }
        }
    }
}
