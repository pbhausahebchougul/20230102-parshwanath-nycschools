//
//  WebserviceConfig.swift
//  NYC Schools App
//
//  Created by Bhausaheb Chougule, Parshwanath on 19/05/23.
//

import Foundation

/// HTTPMethodTypes
public enum HTTPMethodType: String {
    case get  = "GET"
    case post = "POST"
    case put  = "PUT"
}

/// Protocol to which every API should confirm to.
protocol APIProtocol {
    func httpMthodType() -> HTTPMethodType
    func apiEndPath() -> String
    func apiBasePath() -> String
}

/// Request Model type that the APIRequestModel confirms to.
protocol APIModelType {
    var api: APIProtocol { get set }
    var parameters: [String: Any]? { get set }
}

/// Request Model that holds every api calls parameters, headers and other api details.
struct APIRequestModel: APIModelType {
    var api: APIProtocol
    var parameters: [String: Any]?

    init(api: APIProtocol, parameters: [String: Any]? = nil) {
        self.api = api
        self.parameters = parameters
    }
}

/// Responsible for generating common headers for requests.
struct WebserviceConfig {
    /// Generates common headers specific to APIs. Can also accept additional headers if demanded by specific APIs.
    ///
    /// - Returns: A configured header JSON dictionary which includes both common and additional params.
    func generateHeader() -> [String: String] { // make compute property
        var headerDict = [String: String]()
        headerDict["Content-Type"] = "application/json"

        return headerDict
    }

    func getRequest(apiModel: APIModelType) -> URLRequest {

        let escapedAddress = (apiModel.api.apiBasePath() + apiModel.api.apiEndPath()).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)

        var request = URLRequest(url: URL(string: escapedAddress!)!)
        request.httpMethod = apiModel.api.httpMthodType().rawValue
        request.allHTTPHeaderFields = self.generateHeader()

        if let params = apiModel.parameters {
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: params as Any, options: .prettyPrinted)
            } catch let error {
                Logger.logDetails(text: error.localizedDescription)
            }
        }

        return request
    }
}
