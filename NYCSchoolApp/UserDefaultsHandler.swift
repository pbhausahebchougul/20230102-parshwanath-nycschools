//
//  UserDefaultsHandler.swift
//  NYCSchoolApp
//
//  Created by Bhausaheb Chougule, Parshwanath on 02/06/23.
//

import Foundation

@propertyWrapper struct UserDefaultsHandler<Value> {
    let key: String
    let defaultValue: Value
    var storage: UserDefaults = .standard

    var wrappedValue: Value {
        get {
            storage.value(forKey: key) as? Value ?? defaultValue
        }
        set {
            storage.set(newValue, forKey: key)
        }
    }
}

extension UserDefaultsHandler where Value: ExpressibleByNilLiteral {
    init(key: String, storage: UserDefaults = .standard) {
        self.init(key: key, defaultValue: nil, storage: storage)
    }
}
