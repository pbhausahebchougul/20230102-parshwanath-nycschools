//
//  SchoolDetailViewModelTests.swift
//  NYCSchoolAppTests
//
//  Created by Bhausaheb Chougule, Parshwanath on 02/06/23.
//

import XCTest
@testable import NYCSchoolApp

final class SchoolDetailViewModelTests: XCTestCase {

    let schoolDetailsViewModel =  SchoolDetailViewModel(serviceRequest: SchoolServiceRequests())

    override class func setUp() {
        Defaults().mockJSON = true
    }

    func testNYCSchoolMockListService() {

        self.callAService(expection: expectation(description: "SATResult"),
                     makeCall: { (completion) in
            SchoolServiceRequests().fetchSchoolSATDetails(dbn: "01M509", localeFileName: "SATResult", completion: completion)
        },
                     testParsing: { (satDetails: [SchoolSATDetails]) in
            self.testSchoolViewModelNotNil(satDetails: satDetails)
        })

    }

    func testSchoolViewModelNotNil(satDetails: [SchoolSATDetails]) {

        self.schoolDetailsViewModel.createCell(satDetails: satDetails)
        XCTAssertNotNil(self.schoolDetailsViewModel.getCellViewModel(at: IndexPath(row: 0, section: 0)))
    }

    func testSchoolSatViewModelNil() {
        XCTAssertTrue(self.schoolDetailsViewModel.getSchoolName().isEmpty)
    }

}
