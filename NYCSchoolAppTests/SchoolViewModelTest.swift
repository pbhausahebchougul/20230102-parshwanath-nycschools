//
//  SchoolViewModelTest.swift
//  NYCSchoolAppTests
//
//  Created by Bhausaheb Chougule, Parshwanath on 02/06/23.
//

import XCTest
@testable import NYCSchoolApp

final class SchoolViewModelTest: XCTestCase {

    let schoolViewModel = SchoolViewModel(serviceRequest: SchoolServiceRequests())

    override class func setUp() {
        Defaults().mockJSON = true
    }

    func testNYCSchoolMockListService() {

        callAService(expection: expectation(description: "NYCSchoolList"),
                     makeCall: { (completion) in
            SchoolServiceRequests().fetchSchools(localeFileName: "NYCSchoolList", completion: completion)
        },
                     testParsing: { (schoolJSON: [School]) in
            self.testSchoolViewModelNotNil(schools: schoolJSON)

        })
    }

    func testSchoolViewModelNotNil(schools: [School]) {

        self.schoolViewModel.createCell(schools: schools)
        XCTAssertNotNil(self.schoolViewModel.schools)
        XCTAssertNotNil(self.schoolViewModel.schools.first?.dbn)
        XCTAssertNotNil(self.schoolViewModel.getCellViewModel(at: IndexPath(row: 0, section: 0)))
        XCTAssertNotNil(self.schoolViewModel.searchSelectedSchool(dbn: "01M509").dbn)
    }

    func testSchoolViewModelNil() {
        XCTAssertTrue(self.schoolViewModel.schools.isEmpty)
        XCTAssertNil(self.schoolViewModel.schools.first?.dbn)
        XCTAssertNil(self.schoolViewModel.getCellViewModel(at: IndexPath(row: 0, section: 0)))
        XCTAssertNil(self.schoolViewModel.searchSelectedSchool(dbn: "").dbn)
    }
}
