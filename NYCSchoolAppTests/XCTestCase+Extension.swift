//
//  XCTestCase+Extension.swift
//  NYCSchoolAppTests
//
//  Created by Bhausaheb Chougule, Parshwanath on 02/06/23.
//

import XCTest

extension XCTestCase {
    typealias MakeSerivceCall<ServiceParseType> = ((Swift.Result<ServiceParseType, Error>) -> Void)
    typealias ServiceCallWrapper<ServiceParseType> = ((@escaping MakeSerivceCall<ServiceParseType>) -> Void)

    /**
     Common code to test loading our model.
     
     - Parameter makeCall: closure that should make a service call
     - Parameter testParsing: closure used after the parsing is done to test the object is parsed correctly
     */
    func callAService<ServiceParseType>(expection: XCTestExpectation,
                                        makeCall: @escaping ServiceCallWrapper<ServiceParseType>,
                                        testParsing: ((ServiceParseType) -> Void)? = nil,
                                        testError: ((Error) -> Void)? = nil) {

        makeCall({ (result: Swift.Result<ServiceParseType, Error>) in
            switch result {
            case .success(let parsedObject):
                if let testParsing = testParsing {
                    testParsing(parsedObject)
                } else {
                    XCTFail("Got success but was not testing success")
                }
            case .failure(let error):
                if let testError = testError {
                    testError(error)
                } else {
                    XCTFail("Got unexpected error: \(error)")
                }
            }

            expection.fulfill()
        })

        waitForExpectations(timeout: 5) { (error) in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
}
