//
//  NYCSchoolSAT_APITest.swift
//  NYCSchoolAppTests
//
//  Created by Bhausaheb Chougule, Parshwanath on 02/06/23.
//

import XCTest
@testable import NYCSchoolApp

final class NYCSchoolSATAPITest: XCTestCase {

    override class func setUp() {
        Defaults().mockJSON = true
    }
    /// Mock test of NYCSchoolSATService with JSON
    func testNYCSchoolListService() {
        callAService(expection: expectation(description: "SATResult"),
                     makeCall: { (completion) in
            SchoolServiceRequests().fetchSchoolSATDetails(dbn: "21K728", localeFileName: "SATResult", completion: completion)
        },
                     testParsing: { (_: [SchoolSATDetails]) in

        })
    }

}
