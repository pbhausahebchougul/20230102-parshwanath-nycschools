//
//  NYCSchoolAPITest.swift
//  NYCSchoolAppTests
//
//  Created by Bhausaheb Chougule, Parshwanath on 02/06/23.
//

import XCTest
@testable import NYCSchoolApp

final class NYCSchoolAPITest: XCTestCase {

    override class func setUp() {
        Defaults().mockJSON = true
    }

    /// Mock test of NYCSchoolListService with JSON
    func testNYCSchoolListService() {
        callAService(expection: expectation(description: "NYCSchoolList"),
                     makeCall: { (completion) in
            SchoolServiceRequests().fetchSchools(localeFileName: "NYCSchoolList", completion: completion)
        },
                     testParsing: { (_: [School]) in

        })
    }

}
